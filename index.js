// Initialise Express.js
const express = require("express");
const app = express();

// Initialise pug
app.set("views", "./views");
app.set("view engine", "pug");
app.use(express.static("./public"));

// Homepage
app.get("/", (req, res) => { res.redirect("/qu1nn"); });

app.get("/qu1nn", (req, res) => {
	res.render("index", { pageTitle: "qu1nn" });
});

// About
app.get("/about", (req, res) => {
	res.render("about", { pageTitle: "about" });
});

// Run app
app.listen(8123);
